package com.project.relevetemperature;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class activity_NewReleve extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, CalendarView.OnDateChangeListener {

    private Button btn_annuler;
    private Button btn_valider;
    private Button btn_calendar;
    private CalendarView calendar;
    private String selectedDate = null;
    public static final int REQUEST_CODE = 11;
    private String[] heure = { "6H", "12H", "18H", "24H"};
    private String selectedHeure;
    private DAOBDD db;
    private releve newReleve = null;
    private EditText saisieTemp;
    private TextView affichageLac;
    private String selectedLac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newreleve);
        btn_annuler = findViewById(R.id.buttonAnnuler);
        btn_annuler.setOnClickListener(this);
        calendar = findViewById(R.id.calendarViewNewReleve);
        btn_calendar = findViewById(R.id.buttonNewRelCalendar);
        btn_calendar.setOnClickListener(this);
        btn_valider = findViewById(R.id.buttonValider);
        btn_valider.setOnClickListener(this);
        calendar.setOnDateChangeListener(this);
        calendar.setVisibility(View.INVISIBLE);
        saisieTemp = findViewById(R.id.editTextNumberDegree);
        Spinner spinHeurRel = (Spinner) findViewById(R.id.spinnerHeureRel);
        spinHeurRel.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,heure);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinHeurRel.setAdapter(aa);
        if (selectedDate == null) {
            final Calendar c = Calendar.getInstance();
            selectedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH).format(c.getTime()); // conversion de la date au format String
            btn_calendar.setText(selectedDate);
            //btn_calendar.setText("No Date");
        }
        affichageLac = findViewById(R.id.textViewTempLac);
        db = new DAOBDD(this);
        db.open();
        selectedLac = db.getLacSelected().getNomLac();
        affichageLac.setText("Entré la température pour le " + selectedLac + " :");
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.buttonAnnuler) {
            db.close();
            finish();
        } else if (view.getId()==R.id.buttonNewRelCalendar) {
            calendar.setVisibility(View.VISIBLE);
        } else if (view.getId()==R.id.buttonValider) {
            setNewReleve();
            db.close();
            finish();
        }
    }
    public void showDatePickerDialog(View v) {
        DialogFragment dateFragment = new DatePickerFragment();
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check for the results
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // get date from string
            selectedDate = data.getStringExtra("selectedDate");
            // set the value of the editText
            btn_calendar.setText(selectedDate);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        Toast.makeText(getApplicationContext(),heure[position] , Toast.LENGTH_LONG).show();
        selectedHeure = heure[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        calendar.setVisibility(View.INVISIBLE);
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selectedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH).format(c.getTime()); // conversion de la date au format
        btn_calendar.setText(selectedDate);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void setNewReleve() {
        String temp = String.valueOf(saisieTemp.getText());
        newReleve = new releve(temp, selectedHeure, selectedDate);
        db.insererReleve(newReleve);
    }
}
