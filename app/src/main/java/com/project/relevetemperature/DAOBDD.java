package com.project.relevetemperature;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOBDD {
    static final int VERSION_BDD =1;
    private static final String NOM_BDD = "bddRelTemp.db";

    // table lac
    static final String TABLE_LAC = "tlac";
    static final String COL_IDLAC = "_id";
    static final int NUM_COL_IDLAC = 0;
    static final String COL_NOMLAC = "NomLac";
    static final int NUM_COL_NOMLAC = 1;
    static final String COL_COORDLAT = "CoordLat";
    static final int NUM_COL_COORDLAT = 2;
    static final String COL_COORDLONG = "CoordLong";
    static final int NUM_COL_COORDLONG = 3;
    static final String COL_SELECTED = "Selected";
    static final int NUM_COL_SELECTED = 4;

    // table releve
    static final String TABLE_RELEVE = "treleve";
    static final String COL_IDRELEVE = "_id";
    static final int NUM_COL_IDRELEVE = 0;
    static final String COL_NOMTEMPERATURE = "Temperature";
    static final int NUM_COL_NOMTEMPERATURE = 1;
    static final String COL_HEURE = "Heure";
    static final int NUM_COL_HEURE = 2;
    static final String COL_DATE = "date";
    static final int NUM_COL_DATE = 3;

    private createBDD tableCourante;
    private Context context;
    private SQLiteDatabase db;

    // constructeur
    public DAOBDD(Context context){
        this.context = context;
        tableCourante = new createBDD(context,NOM_BDD,null,VERSION_BDD);
    }

    //si la bdd n’existe pas, l’objet SQLiteOpenHelper exécute la méthode onCreate
    //si la version de la base a changé, la méthode onUpgrade sera lancée
    // dans les 2 cas l’appel à getWritableDatabase ou getReadableDatabase renverra  la base
    // dedonnées en cache, nouvellement ouverte, nouvellement créée ou mise à jour

    //les méthode d'instance
    public DAOBDD open(){
        db = tableCourante.getWritableDatabase();
        return this;
    }
    public DAOBDD close(){
        db.close();
        return null;
    }

    // pour les lac
    public long insererLac(lac unLac){
        // Création d'un contentValues
        ContentValues values = new ContentValues();
        // on lui associe une valeur associé à une clé
        values.put(COL_NOMLAC, unLac.getNomLac());
        values.put(COL_COORDLAT, unLac.getCoordLat());
        values.put(COL_COORDLONG, unLac.getCoordLong());
        values.put(COL_SELECTED, unLac.getSelected());
        //on insère l'objet dans la BDD via le ContentValues
        return db.insert(TABLE_LAC, null, values);
    }
    public static lac cursorToLAC(Cursor c){
        // Cette méthode permet de convertir un cursor en un client
        // si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0) {
            return null;
        } else { //Sinon
            c.moveToFirst(); // on se place sur le premier élément
            lac unLac = new lac(null,null,null,null);
            unLac.setNomLac(c.getString(NUM_COL_NOMLAC));
            unLac.setCoordLat(c.getString(NUM_COL_COORDLAT));
            unLac.setCoordLong(c.getString(NUM_COL_COORDLONG));
            unLac.setSelected(c.getString(NUM_COL_SELECTED));
            c.close(); // On ferme le cursor
            return unLac; // On retourne le lac
        }
    }

    public lac getLacWithNomLac(String nomlac){
        Cursor c = db.query(TABLE_LAC, new String[] {COL_IDLAC,COL_NOMLAC,COL_COORDLAT, COL_COORDLONG, COL_SELECTED}, COL_NOMLAC + " LIKE \"" + nomlac +"\"", null, null, null, null);
        return cursorToLAC(c);
    }

    public lac getLacWithId(int id){
        Cursor c = db.query(TABLE_LAC, new String[] {COL_IDLAC,COL_NOMLAC,COL_COORDLAT, COL_COORDLONG, COL_SELECTED}, COL_IDLAC + " LIKE \"" + id +"\"", null, null, null, null);
        return cursorToLAC(c);
    }

    public lac getLacSelected() {
        String selected = "true";
        Cursor c = db.query(TABLE_LAC, new String[] {COL_IDLAC,COL_NOMLAC,COL_COORDLAT, COL_COORDLONG, COL_SELECTED}, COL_SELECTED + " LIKE \"" + selected +"\"", null, null, null, null);
        return cursorToLAC(c);
    }

    public Cursor getDataLac(){
        return db.rawQuery("SELECT * FROM tlac",null);
    }

    // pour les releve
    public long insererReleve(releve unReleve) {
        // Création d'un contentValues
        ContentValues values = new ContentValues();
        // on lui associe une valeur associé à une clé
        values.put(COL_NOMTEMPERATURE, unReleve.getTemp());
        values.put(COL_HEURE, unReleve.getHeure());
        values.put(COL_DATE, unReleve.getDate());
        //on insère l'objet dans la BDD via le ContentValues
        return db.insert(TABLE_RELEVE, null, values);
    }
    public static releve cursorToRELEVE(Cursor c){
        // Cette méthode permet de convertir un cursor en un releve
        // si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0) {
            return null;
        } else { // Sinon
            c.moveToFirst(); // on se place sur le premier élément
            releve unReleve = new releve(null,null,null);
            unReleve.setTemp(c.getString(NUM_COL_NOMTEMPERATURE));
            unReleve.setHeure(c.getString(NUM_COL_HEURE));
            unReleve.setDate(c.getString(NUM_COL_DATE));
            c.close(); // On ferme le cursor
            return unReleve; // On retourne le releve
        }
    }
    public releve getReleveWithTemp(String temp){
        Cursor c = db.query(TABLE_RELEVE, new String[] {COL_IDRELEVE,COL_NOMTEMPERATURE,COL_HEURE,COL_DATE}, COL_NOMTEMPERATURE + " LIKE \"" + temp +"\"", null, null, null, null);
        return cursorToRELEVE(c);
    }
    public releve getReleveWithId(int id){
        Cursor c = db.query(TABLE_RELEVE, new String[] {COL_IDRELEVE,COL_NOMTEMPERATURE,COL_HEURE,COL_DATE}, COL_IDRELEVE + " LIKE \"" + id +"\"", null, null, null, null);
        return cursorToRELEVE(c);
    }

    public Cursor getDataReleve() { return db.rawQuery("SELECT * FROM treleve", null); }
}
