package com.project.relevetemperature;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class createBDD extends SQLiteOpenHelper {
    private static final String TABLE_LAC = "tlac";
    static final String COL_IDLAC = "_id";
    private static final String COL_NOMLAC = "NomLac";
    private static final String COL_COORDLAT = "CoordLat";
    private static final String COL_COORDLONG = "CoordLong";
    private static final String COL_SELECTED = "Selected";

    private static final String CREATE_TABLE_LAC = "CREATE TABLE " + TABLE_LAC + "("+ COL_IDLAC +" INTEGER PRIMARY KEY AUTOINCREMENT,"+ COL_NOMLAC + " TEXT NOT NULL," + COL_COORDLAT + " TEXT NOT NULL, " + COL_COORDLONG + " TEXT NOT NULL," + COL_SELECTED + " TEXT NOT NULL);";

    private static final String TABLE_RELEVE = "treleve";
    static final String COL_IDRELEVE = "_id";
    private static final String COL_NOMTEMPERATURE = "Temperature";
    private static final String COL_HEURE = "Heure";
    private static final String COL_DATE = "date";

    private static final String CREATE_TABLE_RELEVE = "CREATE TABLE " + TABLE_RELEVE + "("+ COL_IDRELEVE +" INTEGER PRIMARY KEY AUTOINCREMENT,"+ COL_NOMTEMPERATURE + " TEXT NOT NULL," + COL_HEURE + " TEXT NOT NULL, " + COL_DATE + " TEXT NOT NULL);";

    //constructeur paramétré
    public createBDD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    //méthodes d'instance permettant la gestion de l'objet
    @Override
    public void onCreate(SQLiteDatabase db) {
        //appelée lorsqu’aucune base n’existe et que la classe utilitaire doit encréer une
        //on créé la table à partir de la requête écrite dans la variableCREATE_BDD
        db.execSQL(CREATE_TABLE_LAC);
        db.execSQL(CREATE_TABLE_RELEVE);
    }
    // appelée si la version de la base a changé
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut supprimer la table et de la recréer
        db.execSQL("DROP TABLE " + TABLE_LAC + ";");
        db.execSQL("DROP TABLE " + TABLE_RELEVE + ";");
        onCreate(db);
    }
}
