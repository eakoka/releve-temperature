package com.project.relevetemperature;

public class lac {
    protected String NomLac;
    protected String coordLat; //coordonnés du lac Latitude
    protected String coordLong; //coordonnés du lac Longitude
    protected String selected;

    public lac(String nomLac, String coordLat, String coordLong, String selected) {
        this.NomLac = nomLac;
        this.coordLat = coordLat;
        this.coordLong = coordLong;
        this.selected = selected;
    }

    public String getNomLac() {
        return NomLac;
    }

    public void setNomLac(String nomLac) {
        NomLac = nomLac;
    }

    public String getCoordLat() {
        return coordLat;
    }

    public void setCoordLat(String coordLat) {
        this.coordLat = coordLat;
    }

    public String getCoordLong() {
        return coordLong;
    }

    public void setCoordLong(String coordLong) {
        this.coordLong = coordLong;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }
}
