package com.project.relevetemperature;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    //public Class activity = DatePickerFragment.class;

    //public void setActivity(Class activity) {
    //    this.activity = activity;
    //}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Utilisation de la date du jour comme date par default
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Creation de l'instance du DatePicker et retournement de celle ci
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        String selectedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH).format(c.getTime()); // conversion de la date au format String
        // retournement du resultat dans l'activity qui a fait appel au fragment
        //getTargetFragment().onActivityResult(
        //        getTargetRequestCode(),
        //        Activity.RESULT_OK,
        //        new Intent().putExtra("selectedDate", selectedDate)
        //);
    }
}
