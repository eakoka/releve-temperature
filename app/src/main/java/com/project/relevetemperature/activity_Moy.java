package com.project.relevetemperature;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class activity_Moy extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private String[] mois = { "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};
    private String selectedMois;
    private Button btn_retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moy);
        Spinner spinMoy = (Spinner) findViewById(R.id.spinner_Moy);
        spinMoy.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,mois);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMoy.setAdapter(aa);
        btn_retour = findViewById(R.id.buttonRetourMoy);
        btn_retour.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonRetourMoy) {
            finish();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        Toast.makeText(getApplicationContext(),mois[position] , Toast.LENGTH_LONG).show();
        selectedMois = mois[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // TODO Auto-generated method stub
    }
}
