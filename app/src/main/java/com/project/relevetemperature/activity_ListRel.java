package com.project.relevetemperature;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class activity_ListRel extends AppCompatActivity implements View.OnClickListener {

    private Button btn_retour;
    private Button btn_calendar;
    private String selectedDate = null;
    public static final int REQUEST_CODE = 11;
    private TextView title;
    private String selectedLac;
    private DAOBDD db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listrel);
        btn_retour = findViewById(R.id.buttonRetour);
        btn_retour.setOnClickListener(this);
        btn_calendar = findViewById(R.id.buttonCalendarListRel);
        btn_calendar.setOnClickListener(this);
        if (selectedDate == null) {
            final Calendar c = Calendar.getInstance();
            selectedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH).format(c.getTime()); // conversion de la date au format String
            btn_calendar.setText(selectedDate);
            //btn_calendar.setText("No Date");
        }
        db = new DAOBDD(this);
        title = findViewById(R.id.textViewTitle);
        db.open();
        selectedLac = db.getLacSelected().getNomLac();
        title.setText("Liste des Relevé pour le " + selectedLac + " :");
        db.close();
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        //boolean newActivity = false;
        if (view.getId()==R.id.buttonRetour) {
            finish();
        } else if (view.getId()==R.id.buttonCalendarListRel) {
            intent = new Intent(activity_ListRel.this, activity_calendar.class);
            startActivityForResult(intent,0);
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment dateFragment = new DatePickerFragment();
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check for the results
        super.onActivityResult(requestCode, resultCode, data);
        //if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // get date from string
        //    selectedDate = data.getStringExtra("selectedDate");
            // set the value of the button
        //    btn_calendar.setText(selectedDate);
        if (resultCode == 1) {
            selectedDate = data.getStringExtra("selectedDate"); // recuperation de la date depuis l'activity calendar
            btn_calendar.setText(selectedDate);
        }
    }
}
