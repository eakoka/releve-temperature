package com.project.relevetemperature;

public class releve {
    protected String temp;
    protected String heure;
    protected String date;

    public releve(String temp, String heure, String date) {
        this.temp = temp;
        this.heure = heure;
        this.date = date;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
