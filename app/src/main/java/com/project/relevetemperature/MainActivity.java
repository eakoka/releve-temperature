package com.project.relevetemperature;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import android.location.Location;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener,AdapterView.OnItemSelectedListener {

    Location currentLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    private Button btn_newRel;
    private Button btn_listRel;
    private Button btn_moy;
    private Marker marker;
    private DAOBDD db;
    String[] lac = {"lac de Grand Lieu","lac de La Maulde","lac d'Herculat"};
    private int position;
    private lac selectedLac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_layout);
        db = new DAOBDD(this);
        db.open();
        selectedLac = db.getLacSelected();
        if (selectedLac==null) {
            firstRun();
        } else {
            mainMenu();
        }
    }
    public void mainMenu() {
        setContentView(R.layout.activity_main);
        btn_newRel = findViewById(R.id.buttonNewReleve);
        btn_listRel = findViewById(R.id.buttonListeReleve);
        btn_moy = findViewById(R.id.buttonMoyMois);
        btn_newRel.setOnClickListener(this);
        btn_listRel.setOnClickListener(this);
        btn_moy.setOnClickListener(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();
        //db.close(); // commenter pour debeugué avec Android Studio database Inspector
    }
    @Override
    public void onClick(View view) {
        Intent intent = null;
        Boolean newActivity = true;
        if (view.getId()==R.id.buttonNewReleve) {
            intent = new Intent(this, activity_NewReleve.class);
        } else if (R.id.buttonListeReleve==view.getId()){
            intent = new Intent(this, activity_ListRel.class);
        } else if (R.id.buttonMoyMois==view.getId()) {
            intent = new Intent(this, activity_Moy.class);
        } else if (R.id.buttonFirstRun==view.getId()) {
            newActivity = false;
            insertSelected();
            mainMenu();
        }
        if (newActivity) {
            startActivity(intent);
        }
    }
    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment)
                            getSupportFragmentManager().findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(MainActivity.this);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng position = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(position);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 5));
        marker = googleMap.addMarker(markerOptions);
        LatLng markLac = new LatLng(Double.parseDouble(selectedLac.getCoordLat()), Double.parseDouble(selectedLac.getCoordLong()));
        //marker.setPosition(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
        marker.setPosition(markLac);
        marker.setTitle("point de relevé");
        marker.setVisible(true);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        Toast.makeText(getApplicationContext(),lac[position] , Toast.LENGTH_LONG).show();
        this.position = position;
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    public void firstRun() {
        setContentView(R.layout.activity_firstrun);
        Spinner spinLac = (Spinner) findViewById(R.id.spinner_FirstRun);
        Button btn_ok = findViewById(R.id.buttonFirstRun);
        btn_ok.setOnClickListener(this);
        spinLac.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,lac);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinLac.setAdapter(aa);
    }
    public void insertSelected() {
        if (position==0) {
            selectedLac = new lac("Lac de Grand lieu", "47.094035", "-1.6431637", "true");
        } else if (position==1) {
            selectedLac = new lac("Lac de la Maulde", "45.780851", "1.8548713", "true");
        } else if (position==2) {
            selectedLac = new lac("Lac d'Herculat", "46.354937", "2.363061", "true");
        }
        db.insererLac(selectedLac);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}